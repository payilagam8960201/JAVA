package LearnTypecast;

public class TypeCastDemo {

	public static void main(String[] args) {
		TypeCastDemo tc=new TypeCastDemo();
		
		tc.display((byte)1000000000);
		

	}

	private void display(int no) {
		System.out.println("int");
		
	}
	private void display(byte no) {
		System.out.println("byte");
		
	}
	private void display(short no) {
		System.out.println("short");
		
	}
	private void display(long no) {
		System.out.println("long");
		
	}
	private void display(float no) {
		System.out.println("float");
		
	}
	private void display(double no) {
		System.out.println("double");
		
	}

}
