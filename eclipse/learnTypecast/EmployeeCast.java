package LearnTypecast;

public class EmployeeCast extends StudentCast {

	public static void main(String[] args) {
//		EmployeeCast emp=new EmployeeCast();
//		StudentCast s=emp;//up cast
//		//s.doProject();
//		s.study();
		StudentCast ss=new EmployeeCast();
		EmployeeCast e=(EmployeeCast)ss;//down cast
		e.study();
		e.doProject();

		
	}
          public void doProject() {
        	  System.out.println("doProject");
        	  
          }
}
