package LearnTypecast;

public class BoxingChild extends BoxingParent {

	
	public static void main(String[] args) {
		BoxingChild s=new BoxingChild();
		BoxingChild s2=new BoxingChild();
		//s.display(s2);
		s.display(23);//autoBoxing 
		s.display((Integer)23);//boxing
	}
	
	private void display(int ob) { //auto Unboxing
		System.out.println("object"+ob);
		char ch1='a';
	   Character ch ='c';//auto boxing
	    Character ch2=Character.valueOf('c');//boxing
		ch1=ch; //auto unboxing
		
	}


//	private void display(Object ob) {//auto boxing
//		// TODO Auto-generated method stub
//		
//	}

	


	}


