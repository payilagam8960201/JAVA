package LearnString;

public class StringMethod {

	public static void main(String[] args) {
		
		  String name ="kavitha";
   	   char[] ar=new char[name.length()];
   	   for(int i=0;i<ar.length;i++) {
   		   ar[i]=name.charAt(i);
   	   }
   	   for (char c : ar) {
			System.out.println(c);
		}
   	   
   	  String b="";
   	  b=String.join("/","20","30","40");
   	  System.out.println(b);
		String name1 = "Narmatha";
		String name2 = "narmatha";
		System.out.println(name1.hashCode());// int
		System.out.println(name1.charAt(0));// char
		System.out.println(name1.length());// int
		// name1=name1.concat("R");  
		System.out.println(name1);
		System.out.println(name1.compareTo(name2));
		System.out.println(name1.compareToIgnoreCase(name2));
		System.out.println(name1.contains("mat"));
		System.out.println(name1.endsWith("matha"));
		System.out.println(name1.startsWith("Nar"));
		System.out.println(name1.indexOf("v"));// from first//not having letter(-1)
		System.out.println(name1.lastIndexOf("a"));// from last
		if(name2.indexOf("A")!=-1) {
			System.out.println("present");
		}
		else {
			System.out.println("not present");
		}
	String n="hi all good morning";
		//System.out.println(n.isEmpty());
		//System.out.println(n.isBlank());
		String[] words= n.split(" ");
		//System.out.println(words.length);//System.out.println(words.length);

		for(int i=0;i<words.length;i++) {
			System.out.print(words[i]+" ");
			}
		
		
		String word=" samsam ";
		System.out.println(word.replace("am","pm"));
		System.out.println(word);//we didn't reinitialize
		System.out.println(word.replaceAll("sam","ram"));
		System.out.println(word.replaceFirst("sam", "ram"));
		System.out.println(word.strip());//removing all whitspace 
		System.out.println(word.stripLeading()+"--");//front space
		System.out.println(word.stripTrailing()+"--");//last space
		System.out.println(word.trim());
		String m='\u0020'+" sam";//remove space lesser than or equal to ('\u0020')space unicode
		System.out.println(m.trim());
		
		
		
		
		int no=2000;
		//String s=no+""; //method 1 to find string length
		//System.out.println(s.length());
		String s=String.valueOf(no);//method 2
		System.out.println(s.length());
		
		int c=countofdigit(no);//method 3
		}
	private static int countofdigit(int no) {
		int count=0;
		while(no>0) {
			no=no/10;
			count++;
		}
		System.out.println(count);
		return count;
	}
	

}
