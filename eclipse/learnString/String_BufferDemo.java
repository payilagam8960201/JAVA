package LearnString;
public class String_BufferDemo {

	public static void main(String[] args) {
		//String s="";
		StringBuffer sb =new StringBuffer("");
		System.out.println(sb.hashCode());
		for(char i='A';i<='Z';i++) {
			//s=s+i;
			sb.append(i);//Adding at the end and it will reassign the value itself
		}
		//System.out.println(s.hashCode());
		System.out.println(sb);
		System.out.println(sb.hashCode());
		
		
		String s="abc";
		s.toUpperCase();
		System.out.println(s);
		
		StringBuffer sb1=new StringBuffer("abcd");
		sb1.reverse();
		System.out.println(sb1);
		sb1.append(10);
		System.out.println(sb1);
		sb1.insert(2, false);
		System.out.println(sb1);
		System.out.println('\''+"ghg"+'\"');
		
		StringBuffer sb2=new StringBuffer();//default capacity 16 more than that it will expand.
		System.out.println(sb2.capacity());
		sb2.ensureCapacity(0);//tbd
		
		System.out.println(sb.capacity());
		
	}

}
