package LearnString;

public class StringDemo {

	public static void main(String[] args) {
		//string is immutable and constant
		//string is a class
		//string is a final class
		
	 String s="abc";
		System.out.println(s.hashCode());
		s="bcd";
		System.out.println(s.hashCode());
        System.out.println(s); 
        System.out.println("---------------");
        for(char i='a';i<'z';i++) {
        	s=s+i;
        	System.out.println(i);
        	//System.out.println(s.hashCode());
        	
        }
        System.out.println("---------------");
        String l="movie";
        System.out.println(l.hashCode());
        String l1="Play";
        System.out.println(l1.hashCode());
        String l2="play";
        System.out.println(l2.hashCode());
        
        
        
        
        String l3="movie";
        System.out.println(l3.hashCode());
        System.out.println("---------------");
        //ASCII value-->a=97,A=65.. diff 32
        char ch='A'+32;
        		System.out.println(ch);
        System.out.println("---------------");
       for(char i='a';i<'z';i++) {
    	   System.out.println((char)(i-32));
    	   
    	   
    	 
       }
	}
}



