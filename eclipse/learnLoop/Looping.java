package LearnLoop;

public class Looping {

	public static void main(String[] args) {
		Looping loop = new Looping();
		// loop.call1();
		// loop.call2();
		//loop.call3();
		//loop.call4();
		//loop.call5();
		 //loop.call6();
		 //loop.call7();
		//loop.call8();
		//loop.call9();
		//loop.call10();
		//loop.call11();
		//loop.task1();
		//loop.task2();
		//loop.task3();
		//loop.task4();
		loop.task5();
	}

	/*
	 * if(count<5) { System.out.println(1); count=count+1;//0+1=1 } if(count<5)
	 * {//1<5 System.out.println(1); count=count+1;//1+1=2 }
	 * 
	 * if(count<5) {//2<5 System.out.println(1); count=count+1;//2+1=3 }
	 * 
	 * if(count<5) {//3<5 System.out.println(1); count=count+1;//3+1=4 }
	 * 
	 * if(count<5) {//4<5 System.out.println(1); count=count+1;//4+1=5 } if(count<5)
	 * {//5<5 System.out.println(1); count=count+1; }
	 */
	public void call1() { // whileloop +1 no
		for(int count = 0;count < 10;count = count + 2)
		{
			System.out.println(count);
			}
	}

	// System.out.println("end");
	public void call2() {// while +2 no
		for(int num = 1;num < 10;num = num + 2)
		{
			System.out.print(num+" ");
			
		}
	}

	public void call3() {// even
		int no = 1;
		while (no <= 100) {
			if (no % 2 == 0) {
				System.out.println(no+" even number");
			}
			
			no = no + 1;
		}

	}

	public void call4() {// odd
		int n = 1;
		while (n <= 100) {
			if (n % 2 != 0) {
				System.out.println(n);
			}
			n = n + 1;
		}
	}

	public void call5() {// boolean and condition checking
		boolean con = true;// (it will compile but not run,true infinity)
		while (con) {
			System.out.println("hi");
		}
		// if(false) { //false inside condition can"t run
		// System.out.println("hi");
		// }

	}

	public void call6() {// break statement
		int no = 1;
		while (true) {
			if (no == 50) {
				break;
			}
			System.out.println(no);
			no = no + 1;
		}
	}

	public void call7() {// continue statement
		int no = 0;
		while (no <= 10) {
			if (no == 5) {
				no++;// no=n0+1
				continue;
			}
			System.out.println(no);
			no++;
		}
	}

	public void call8() {// for loop
		for (int count = 0; count < 10; count = count + 1)//reading-initial->condition->print->(increment->condition-loop)
		// for(;;) //for(;true;) //everything in while loop can be done in for loop
		// {System.out.println("hi");}//infinity loop
		{
			System.out.println(count);
		}
	}
	
	//11111
	//11111
	//11111
	public void call9() { //nested while
		int row = 1;
		while(row<=3) {
		int column = 0;
		while (column < 5) {
			System.out.print(1+" ");
			column = column + 1;
		}
		System.out.println();
		row=row+1;
		}
	}

	
	
	
	public void call10() { // nested for
		for (int row = 0; row < 3; row = row + 1) {
			for (int column = 0; column < 5; column = column + 1) {
				System.out.print(1 + "");
			}
			System.out.println();

		}
	}

	public void call11() {//do while
		 int num=4;
			do {
				System.out.println("hi");
				num=num+1;
			}while(num<=5);
	}
	
	
	//task 1
		//1 1 1 1 1
		//2 2 2 2 2
		//3 3 3 3 3
	public void task1() {
		for (int row = 1; row <= 3; row = row + 1) {
			for (int column = 1; column <= 5; column = column + 1) {
				System.out.print(row + " ");
			}
			System.out.println();
		}
	}
//	 
	
	//task2
		//0 2 4 6 8
		//0 2 4 6 8
		//0 2 4 6 8
	
	
	public void task2() {
		System.out.println(" ");
		for (int row = 0; row < 3; row = row + 1) {
			for (int column = 0; column <= 8; column = column + 1) {
				if (column % 2 == 0) {
					System.out.print(column + " ");
				}
			}
			System.out.println();
		}
	}
	
	//task3
		//* * * * *
	    //* * * * *
	    //* * * * *
	

	public void task3() {
		System.out.println(" ");
		for (int row = 0; row < 3; row = row + 1) {
			for (int column = 0; column < 5; column = column + 1) {
				System.out.print("* ");
			}
			System.out.println();
		}
	}
	//task 4
		//5 4 3 2 1
	    //5 4 3 2 1
	    //5 4 3 2 1

	
	public void task4() {
		System.out.println(" ");
		for (int row = 0; row < 3; row = row + 1) {
			for (int column = 5; column >= 1; column = column - 1) {
				System.out.print(column + " ");
			}
			System.out.println();
		}
	}
	//task 5
	//*
	//**
	//***
	//****
	//*****
	
	public void task5() {
		int num=1;
		for (int row = 0; row < 3; row++) {
			for (int column = 0; column <=row; column++) {
				System.out.print(num+" ");
				num++;
			}
			System.out.println(" ");
		}
		System.out.println("");
	
		
		
		
		
		
	//* * * * *
	//* * * *
    //* * * 
	//* * 
	//*
		
int n=5;
		for (int row = n; row >= 1; row--) {

			for (int column = 1; column <=row; column++) {

				System.out.print(column );
			}
			System.out.println();
		}
		System.out.println(" ");
		
		//* * * * *
		//  * * * *
	    //    * * *
		//      * * 
		//        *
		
		
int n1=5;
		for (int row = n1; row >= 1; row--) {

			for (int column =n1; column >=row; column--) {

				System.out.print("  ");                          
			}
			for (int column1 = 1; column1 <= row; column1++) 
				{  
				System.out.print("* ");  
				}  
			
			System.out.println();
		}
		
		
		for (int row = 0; row < 5; row++) {
			  for (int column =0; column<5; column++) {
			if((row==2)||(column==2)){
				System.out.print("* ");
			}
			else {
				System.out.print("  ");
				}
			}
			  System.out.println("");
			  }	
		
		System.out.println();
		
		for (int row = 0; row < 5; row++) {
			  for (int column =0; column<5; column++) {
			if((row==column)||(row+column==4)||(column==2)||(row==2)){//X pattern-0==0 1==1 2==2 3==3 4==4,(0+4 1+3 2+2 3+1 4+0)
				System.out.print("* ");
			}
			else {
				System.out.print("  ");
				}
			}
			  System.out.println("");
			  }
		
		
		}
}




