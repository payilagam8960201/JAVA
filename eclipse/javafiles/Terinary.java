package Javafiles;

public class Terinary {

	public static void main(String[] args) {

		int a = 10;
		int b = 20;
		int c = 30; // variable=(condition)?"True":"false";(terinary operator).....
		String result = (a > b) ? (a > c) ? "a is greater" : "second no" : "first no";
		System.out.println(result);

//System.out.println("(a>b)?(a>c)?"a is greater":"2":"1"");......

		/*
		 * int mark=80; int internal=20; String
		 * marks=(mark>=80)?(internal>=20)?"pass":"internal fail":"mark fail";
		 * System.out.println(marks); }
		 */

		int mark = 80;
		int internal = 20;
		int attendence = 75;
		String marks = ((mark >= 80)? ((internal >= 20) ? ((attendence >= 75) ? "pass" : "attendence fail") : "internal fail"): "mark fail");
		// String marks= ((mark>=80)? ((internal>=20)?
		// ((attendence>=75)?"pass":"attendence fail") :"internal fail") :"mark fail");
		System.out.println(marks);

		int day = 4;//switch case
		switch (day) {
		case 0:
			System.out.println("Sunday");
			break;
		case 1:
			System.out.println("Monday");
			break;
		case 2:
			System.out.println("Tuesday");
			break;
		default: //like else-first and last statement
			System.out.println("nothing");
			break;
		case 3:
			System.out.println("Wednesday");
			break;
		case 4:
			System.out.println("Thursday");
			break;
		case 5:
			System.out.println("Friday");
			break;
		case 6:
			System.out.println("Saturday");
			break;
		
		}
	}
}
