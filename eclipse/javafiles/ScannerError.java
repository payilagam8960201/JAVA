package Javafiles;

import java.util.Scanner;

public class ScannerError {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("no1");
		int no1 = sc.nextInt();
		System.out.println("no2");
		int no2 = sc.nextInt();
		while (no1 <= no2) {
			System.out.println(no1);
			no1++;
		}
		System.out.println("Enter name:");
		String name = sc.next();
		System.out.println("welcome " + name);
        sc.close();//to close the scanner object
        
	}

}
