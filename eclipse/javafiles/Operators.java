package Javafiles;

public class Operators {

	public static void main(String[] args) {
		Operators op=new Operators();
		System.out.println(op instanceof Operators);//to check which class the ob is
		//op.post();
		//op.pre();
		//op.and();
		//op.or();
		//op.not();
		  op.bit();
	}
	public void post() {
		//post unary operators
		int no=10;
		System.out.println(no);
		no++;//n0=n0+1 post increment
		System.out.println(no);
		no--;//no=no-1 post decrement
		System.out.println(no);
		no+=2;//no=n0+2
		System.out.println(no);
		no-=2;//n0=n0-2
		System.out.println(no);
		no/=2;//no=n0/2
		System.out.println(no);
		no*=2;//no=no*2
		System.out.println(no);
		
		

	int no2=5;
	System.out.println(" ");
	  System.out.println(no2++);//5 -> 5+1=6
	  System.out.println(no2);//6
	  
	  int no3=5;
	  System.out.println(no3--);//5
	  System.out.println(no3);//4
	  
	  int no4= 5;
	  int no5 = 3;
	  int no6 = no4++ - no5-- + no5 + no4;
	    // 5-3+2+6;
	    //2+2+6
	    //10
	    //no4=6; no5=2;
	  System.out.println(no6);//18 9 16 12 10
	}
	public void pre() { //pre increment
	int no=5;
	  System.out.println(no);//5
	  System.out.println(++no);//6
	  System.out.println(no);//6
	  
	  int no2=5;
	  System.out.println(no2);//5 //pre decrement
	  System.out.println(--no2);//4
	  System.out.println(no2);//4
	  
	  int no3=10;
	  int no4=5;
	  
	  int no5 = --no4 - --no3 + --no4 - no3 + no4++ - no3-- + no4;
	    //4-9+3-9+3-9+4
	    //14-27
	    //-13
	         //no3 = 8 
	     //no4 =4
	  System.out.println(no5);//23 -7 -2 -10 -13 -17
	  System.out.println(no4);//4  10  4  4   4   3
	  System.out.println(no3);//9  8   8  9   9   8
	}
	
	
public void and() { //and gate
int  no1=10;
  int no2=5;
  
  if(no1==9 && no2==6) {
   System.out.println("hi");
  }else {
   System.out.println("bye");
  }
  
  if(no1++ == 10 && --no2==5) {//interview qn
	  System.out.println(" ");
	   System.out.println("hello");
	  }
	  //10==9
	  //no1=11
	  System.out.println(no1); //11
	  
	  System.out.println(no2);//4

}
public void or() { //or gate
int  no1=10;
int no2=5;

if(no1++ == 9 || --no2==5) {
 System.out.println("hello");
}
//10==9
//no1=11
System.out.println(no1); //11
System.out.println(no2);//4

}

public void not() { //not
	int no1 = 10;
	int no2 = 5;
	if (no1 != 9) {// statement true print otherwise not
		System.out.println("hi");

		if (no1++ == 9 || --no2 == 5) {
			System.out.println("hello");
		}
		// 10==9
		// no1=11
		System.out.println(no1); // 11
		System.out.println(no2);// 4

	}
}
//bitwise operator
public void bit() {
	int no=5;//'~'
	System.out.println(~no);//-6
	//int no1=2;//left shift...ADDING ZERO IN LEFT
	System.out.println(2<<1);//4
	//int no2=2;//right shift..ADDING ZEROS IN RIGHT
	System.out.println(2>>2);//0
	System.out.println(2&3);//2-bitwise and
	System.out.println(2|3);//3-bitwise OR
	System.out.println(2^3);//1-bitwise XOR
	System.out.println(0X2222);//8738-hexadecimal
}


	}


   