package learnCollection;

//public class ArrayListDemo {
//	
//	public static void main(String[] args) {
//		ArrayListDemo al=new ArrayListDemo ();
//	display(10);
//	display(10.5f);
//	display("sdf");
//	
//		
//
//	}
//
//	private static void display(Object i) {
//		// TODO Auto-generated method stub
//		
//	}
	
	
	import java.util.ArrayList;

	public class ArrayListDemo {

	 public static void main(String[] args) {
	  
	  ArrayList al = new ArrayList();
	  int no = 10;
	  al.add(no);
	  al.add(20);
	  al.add(25);
	  al.add(87);
	  al.add("abc");
	  al.add(false);
	  al.add(10.5f);
	  al.add('c');
	  //wrapper class
	  
	  //int = Integer
	  //byte = Byte
	  //short = Short
	  //long = Long
	  //float = Float
	  //double = Double
	  //char = Char
	  //boolean = "Boolean"
	  
	  int no2 = 10;
	  Integer i = no2;
	  i=no2;//AutoBoxing  primitive to wrapper class
	  no2=i;//AutoUnBoxing wrapper class to primitive
	  
	  boolean b = true;
	  Boolean b2 = b;
	  display(10);
	  display(10.5f);
	  ArrayListDemo ad = new ArrayListDemo();
	  display(ad);
	  
	  
	 }
	 public static void display(Object i) {
		 System.out.println(i);
	  
	 }

	 

	 

	}
