package LearnArray;

import java.util.Scanner;

public class ArrayTask {
	public static void main(String[] args) {

		ArrayTask fn = new ArrayTask();
		int[] marks = fn.getArray();
		int res = fn.getInput();

		boolean rep = true;
		while (rep) {
			if (res < 1 || res > 4) {
				res = fn.getInput();
			} else {
				rep = false;

				fn.findResult(marks, res);
			}
		}
	}

	private int[] getArray() {
		int[] marks = new int[5];
		String[] sub = { "tam", "eng", "mat", "sci", "ss" };
		Scanner sc = new Scanner(System.in);
		for (int i = 0; i < 5; i++) {
			System.out.println("Enter mark " + sub[i]);
			marks[i] = sc.nextInt();
		}
		return marks;
	}

	private int getInput() {
		Scanner sc = new Scanner(System.in);
		System.out.println("if you want max enter 1");
		System.out.println("if you want min enter 2");
		System.out.println("if you want total enter 3");
		System.out.println("if you want avg enter 4");
		int res = sc.nextInt();
		return res;
	}

	private void findResult(int[] marks, int res) {
		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;
		int total = 0;
		int avg = 0;
		for (int i = 0; i < marks.length; i++) {
			if (res == 1) {
				if (max < marks[i]) {
					max = marks[i];
				}
				
			} else if (res == 2) {
				if (min > marks[i]) {
					min = marks[i];
				}
			} else if (res == 3) {
				total = total + marks[i];
			} else if (res == 4) {
				total = total + marks[i];
				avg = total / marks.length;
			}
		}
		if (res == 1) {
			System.out.println("Maximum Mark : " + max);
		} else if (res == 2) {
			System.out.println("Minimum Mark : " + min);
		} else if (res == 3) {
			System.out.println("Total Mark : " + total);
		} else if (res == 4) {
			System.out.println("Average : " + avg);
		}
	}
}