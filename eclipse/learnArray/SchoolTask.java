package LearnArray;

public class SchoolTask {
	public static void main(String[] args) {
		SchoolTask obj = new SchoolTask();
		String[] names = { "X", "Y", "Z" };
		String[] rank = { "First", "Second", "Third" };
		int[][] marks = { { 45, 78, 98, 90, 60 }, { 100, 100, 65, 100, 100 }, { 100, 100, 100, 100, 100 } };
		int[] stud = obj.total(marks, names);
		obj.topper(stud, names);
		obj.rank(stud, rank);
	}

	private void rank(int[] stud, String[] rank) {
		// for(int i=0;i<3;i++) {
		// System.out.println( stud[i]);}
		System.out.println("Rank");
		int temp = 0;
		for (int i = 0; i < stud.length; i++) {
			for (int j = i + 1; j < stud.length; j++) {
				if (stud[i] < stud[j]) {
					temp = stud[i];
					stud[i] = stud[j];
					stud[j] = temp;
				}
			}
			System.out.println(rank[i] + "=" + stud[i]);
		}
	}

	public void topper(int[] stud, String[] names) {
		System.out.println("Topper");
		int max = 0;
		int m = 0;
		for (int i = 0; i < stud.length; i++) {

			if (max < stud[i]) {
				max = stud[i];
				m = i;
			}
		}
		System.out.println(names[m] + "=" + max);
	}

	public int[] total(int[][] marks, String[] names) {
		int[] stud = new int[marks.length];
		System.out.println("Total");
		for (int row = 0; row < marks.length; row++) {
			int total = 0;
			for (int col = 0; col < marks[row].length; col++) {
				total = total + marks[row][col];
			}
			System.out.println(names[row] + "=" + total);
			stud[row] = total;
		}
		return stud;
	}
}
