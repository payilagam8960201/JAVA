package LearnException;

public class Sam {

	public static void main(String[] args) {
		Sam sam=new Sam();
		int[] marks= {23,65,75,87,43,13};
		try {
			sam.printArray(marks);
		} catch (ArrayIndexOutOfBoundsException e) {
			
			e.printStackTrace();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
}
public void printArray(int[]marks)throws ArrayIndexOutOfBoundsException
,Exception { //throws-to escape from compile time exception
	for(int i=0;i<6;i++) {
		System.out.println(marks[i]);
		
	}
	
}
}
