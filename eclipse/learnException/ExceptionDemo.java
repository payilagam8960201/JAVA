package LearnException;

import java.util.Scanner;

public class ExceptionDemo {

	 public static void main(String[] args) {
	  Scanner sc = new Scanner(System.in);
	  System.out.println("Enter no1: ");
	  int no1 = sc.nextInt();
	  System.out.println("Enter no2: ");
	  int no2 = sc.nextInt();
	  
	  divide(no1,no2);
	  add(no1,no2); 
	  
	  
	 }

	 private static void add(int no1, int no2) {
	  System.out.println(no1+no2);
	  
	 }

	 private static void divide(int no1, int no2) {
	  try {
	   System.out.println(no1 / no2);//(10/0 -unchecked exception(not checked during compile time-runtime exception))
	   //Runtime Exception Class and its subclass are the only Unchecked exception(jvm)
	   
	   int[] ar=new int[no1];
	   for(int i=0;i<10;i++) {
	   System.out.println(ar[i]);
	   }
	  } catch(Exception m) {
		  System.out.println("abc");
		 //m.printStackTrace();
	  }
	  finally {
		  System.out.println("cjhbhnjk");
	  }
//	  catch (ArithmeticException m) {
//	   System.out.println("check input");
//	  }catch(NegativeArraySizeException n) {
//		  System.out.println("array length");
//	  }
//	  catch(ArrayIndexOutOfBoundsException s) {
//		  System.out.println("out of bound");
//	  }
//	  
	  
	 }

	 
	}