package LearnException;

//public class LowAttendenceException extends RuntimeException {  //unchecked so don't need throws and user defined Exception so throw
//public void allowForExam (int percentage) {
//	if(percentage>=75) {
//		System.out.println("permit");
//		}
//	else{
//		LowAttendenceException lae=new LowAttendenceException();
//		throw lae;
//	}
//}
//	}


public class LowAttendenceException extends Exception {  //checked so throws and user defined Exception so throw
public void allowForExam (int percentage)throws LowAttendenceException {
	if(percentage>=75) {
		System.out.println("permit");
		}
	else{
		//LowAttendenceException lae=new LowAttendenceException();
		//throw lae;
		throw new LowAttendenceException();
	}
}
	}
