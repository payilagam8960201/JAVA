package LearnIf_Else;

public class Cricket {

	public static void main(String[] args) {
		Cricket obj = new Cricket();
		obj.compare(10, 10, 20);
	}

	public void compare(int n1, int n2, int n3) {
		if (n1 > n2) {
			if (n1 > n3) {//nested if
				System.out.println("n1 is greater");
			} else if (n3 > n1) {
				System.out.println("n3 is greater");
			} else {
				System.out.println("n1=n3");
			}
		}

		else if (n2 > n1) {
			if (n2 > n3) {
				System.out.println("n2 is greater");
			} else if (n3 > n2) {
				System.out.println("n3 is greater");
			} else {
				System.out.println("n2=n3");
			}
		}

		else if (n1 > n3) { // n1 == n2
			System.out.println("n1=n2");
		} else if (n3 > n1) { // n1==n2
			System.out.println("n3 is greater");
		} else {
			System.out.println("all are equal");
		}
	}
}
