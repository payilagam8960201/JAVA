public class SuperMarket
{
	//non static variables
	String name;
	int price;
	public static void main(String[] args)//main method(pre defined method)
	{
		//local variables
		SuperMarket prod1=new SuperMarket();
		prod1.name="abc";
		prod1.price=123;
		SuperMarket prod2=new SuperMarket();
		prod2.name="def";
		prod2.price=456;
		int r = prod2.add(); //method calling statement(add-user defined method)
		System.out.println(r);
	} 
	//method defention
	public int add()//method signature
	{    //method body
	 int no1 = 10;
	 int no2 = 20;
	 int result = no1+no2;
	return result;
	}
}