public class Shop{
	//static method
	static String shopname="kavi";
	static int doorNo=282;
	// non static method
	String name;
	int price;
	public static void main(String[] args)//main method
	{
		//local variable
		Shop prod1=new Shop();
		prod1.name="oil";
		prod1.price=180;
		Shop prod2=new Shop();
		prod2.name="soap";
		prod2.price=20;
		Shop prod3=new Shop();
		prod3.name="paper";
		prod3.price=10;
		System.out.println("Shop Name"+ Shop.shopname);
		System.out.println("Shop Door No."+ Shop.doorNo);
		
		System.out.println("product name:"+ prod1.name);
		System.out.println("product price:"+ prod1.price);
		System.out.println("product name:"+ prod2.name);
		System.out.println("product price:"+ prod2.price);
		System.out.println("product name:"+ prod3.name);
		System.out.println("product price:"+ prod3.price);
		
		prod2.buy(); //method Calling Statement
		System.out.println("This is main method");
		}
		public void buy() //method defenition(buy-user defined method)
	{
		System.out.println("This is buy method");	
	}
}