public class Pasanga extends AmmaAppa
{
public Pasanga()
{
super();//implicitily called
System.out.println("pasanga-const");
}
public Pasanga(int b)
{
//super();implicitily called
System.out.println("P-one-argument const ");
}
public static void main(String[] args)
{
Pasanga pp=new Pasanga(10);
//Multilevel Inheritance
pp.work();
pp.motivate();
//pp.takerest();
System.out.println(pp.hashCode());//for every subclass Object class is super class(Object class->java.lang->hashCode method)
//Pasanga pp2=new Pasanga();
//System.out.println(pp2.hashCode());
}
	public void work()
	{
	System.out.println("work");
	}
}