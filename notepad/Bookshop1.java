public class Bookshop1
{
public Bookshop1()//constructor Overloading
{
System.out.println("no-args constructor");
}

public Bookshop1(int no)
{
System.out.println("one-args constructor");
}

public Bookshop1(String str)
{
System.out.println("one-args constructor");
}
public static void main(String args[])
{
Bookshop1 book1 =new Bookshop1();//object
Bookshop1 book2 =new Bookshop1(10);
Bookshop1 book3 =new Bookshop1("xyz");
Bookshop1 book4=new Bookshop1("abc");
}
}