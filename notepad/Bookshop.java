public class Bookshop
{
	//CONSTRUCTOR-return datatype not required,should be same as class name,
	//called automatically when object is created,initializing object specific values
	
	public Bookshop(String bookname,String author,int price)//parameterized constructor
	{                                            
		System.out.println("Details of Book");
		this.bookname=bookname; //using "this" keyword-it refers to the current object
        this.author=author;
		this.price=price;
	}
	
	public Bookshop()//no-args constructor
	{
		System.out.println("abc");
	}
	
	//non-static variable
	String bookname;
	String author;
	int price;
	
public static void main (String[] args)//main method(pre defined method)
{
	//local variable
	Bookshop book1 = new Bookshop("abc","author1",45);//object(or)instance
	//book1.bookname="abc";
	//book1.author="author1";
	//book1.price=45;
	book1.sell();//method Calling Statement(sell-user defined method)
	
    Bookshop book2 = new Bookshop("bcd","author2",55);//object(or)instancepublic class Bookshop
	//book2.bookname="bcd";
	//book2.author="author2";
	//book2.price=55;
	book2.sell();//method Calling Statement(sell-user defined method)
	Bookshop book3 = new Bookshop();
}
	//method defenition
	public void sell()//method signature
	//method body
	{
		this.buy();
	System.out.println("BookName:"+bookname);
	System.out.println("Author:"+author);
	System.out.println("BookPrice:"+price);
	} 
	public void buy()
	{
		//this.sell();//stack overflow error will occur
		System.out.println("this is buy method");
	}
}
		