public final class FinalDemo
{
 final int no = 10;//final-can't define or reinitialize
  static int no1 = 100;
public static void main(String[] args)
{ 
 FinalDemo fd = new FinalDemo();
// fd.no = 15;
 no1 = 99;
 System.out.println(fd.no);
 System.out.println(FinalDemo.no1);
 fd.display();
 
 
}
public final void display()
{ 
 System.out.println("display");
}
  
}